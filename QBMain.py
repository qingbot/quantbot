# -*- coding: utf-8 -*-

import QBDownloader


import QBMonitor
import QBConfigIO
import QBProfitForecast
import QBRanking
import QBSendMail
import QBConstant
import QBLoader


def run():

    #下载当天股价
    obj_dl = QBDownloader.QBDownloaderDayPrice()
    obj_dl.saveto_csv()

    #下载行业信息
    obj_dl = QBDownloader.QBDownloaderIndustry()
    obj_dl.saveto_csv()

    #下载A股股票列表
    obj_dl = QBDownloader.QBDownloaderStockList()
    obj_dl.saveto_csv()
    obj_qbl_sl = QBLoader.QBLoader(QBConfigIO.FILE_PATH_STOCK_LIST, 1)
    df_sl = obj_qbl_sl.loadto_df()
    #下载POOL中股票的历史股价
    obj_qbm = QBMonitor.QBMonitor(df_sl)
    obj_qbm.his_price_of_pool_saveto_csv()

    #存储东财股票盈利预测
    obj_pf = QBProfitForecast.QBProfitForecast()
    df_pf = obj_pf.get_df_profit_forecast()
    print df_pf.head()
    print u'存储成功'

    #根据股票预测盈利情况进行排序
    obj_qbr = QBRanking.QBRanking()
    obj_qbr.rank_df()
    # 根据行业股票预测盈利情况进行排序
    obj_qbr = QBRanking.QBRankingIndustry()
    obj_qbr.rank_df()


    print u'存储成功'
    #发送POOL中股票邮件、预测情况附件
    obj_qbsp = QBSendMail.QBSendMailPool(QBConstant.MAIL_SUBJECT_ATTACH, QBConstant.MAIL_SUBJECT_ATTACH, QBConfigIO.FILE_LIST_MAIL)
    obj_qbsp.send()
    # 发送股票多因子排序邮件
    obj_qbsm = QBSendMail.QBSendMailMultiFactor(QBConstant.MAIL_SUBJECT_MULTIFACTOR, QBConstant.MAIL_SUBJECT_MULTIFACTOR)
    obj_qbsm.send()


if __name__ == '__main__':
    run()
    pass