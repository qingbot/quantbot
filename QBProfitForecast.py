
# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import pandas as pd
import requests
import csv
from time import sleep
import QBConfigIO




class QBProfitForecast(object):
    def __init__(self):
        #定义存放预测数据
        self.__webpage_num = 71
        self.__file_name = QBConfigIO.FILE_PATH_PROFIT_FORECAST
        print u'>>>QBProfitForecast: Initialized.'

        if self.__saveto_csv_profit_forecast(self.__file_name, self.__webpage_num):
            self.__df_profit_forecast = pd.read_csv(self.__file_name)


    def get_df_profit_forecast(self):
        return self.__df_profit_forecast


    def __saveto_csv_profit_forecast(self, filename, pagenum):

        #filename = 'forecast\\forecast' + time.strftime("%Y%m%d", time.localtime()) + '.csv'
        print u'[启动]打开文件：', filename
        with open(filename, 'wb') as csvfile:
            spamwriter = csv.writer(csvfile, dialect='excel')
            csv_header = ['sno', 'code', 'sname', 'close', 'per', 'repnum', 'buy', 'overweight', 'neutral', 'reduce', 'sell', \
                        'e2015', 'e2016', 'pe2016', 'e2017', 'pe2017', 'e2018', 'pe2018', 'b1', 'b2', 'b3']
            print u'[文件]写入表头'
            spamwriter.writerow(csv_header)
        
        #PAGE_NUM = 70
        
        for i in range(1, pagenum):

            url = 'http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=C._A&sty=GEMCPF&st=(AllNum)&sr=-1&p={}'.format(i)
            url = url +'&ps=50&cb=&js=var%20YBIYlkKH={%22data%22:[(x)],%22pages%22:%22(pc)%22}&token=3a965a43f705cf1d9ad7e1a3e429d622&rt=49511416'

            headers = {
                'User-Agent': 'User-Agent:Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50'
                }

            print u'[采集]访问网站：第', i, u'页'
            sleep(1)
            wb_data = requests.get(url, headers=headers)
            soup = BeautifulSoup(wb_data.text, 'lxml')
            body = soup.body.text
            print u'[采集]网页信息读取完毕'
            body1 = body.split('[')[1]
            body2 = body1.split(']')[0]
            body3 = body2.split('\",')
            body_len = len(body3)

            print u'[采集]共采集', body_len, u'条数据'

            with open(filename, 'ab') as csvfile:

                spamwriter = csv.writer(csvfile, dialect='excel')

                for j in range(0, body_len):
                    print u'[文件]写入第', j, u'条数据'
                    body3_line = body3[j].replace('\"', '').replace('-', '0').encode('GBK')
                    #print body3_line.encod
                    #encode('utf-8')
                    body3_line_list = body3_line.split(',')
                    spamwriter.writerow(body3_line_list)
                    print u'[文件]第', j, u'数据写入完毕'

            print u'[文件]本页数据全部写入完毕'
            
        return True

if __name__ == '__main__':

    obj_pf = QBProfitForecast()
    df_pf = obj_pf.get_df_profit_forecast()
    print df_pf.head()
    print u'存储成功'