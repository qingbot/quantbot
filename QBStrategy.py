# -*- coding: utf-8 -*-

#策略交易的基类很简单，就一个onbar函数，响应新的一个tick的数据到达。

import pandas as pd

from abc import ABCMeta, abstractmethod

from QBEvent import SignalEvent

'''
每个策略至少应该实现onbar函数，就是事件loop里，当一个新的tick到达，
就会触发Strategy里的OnBar事件。
'''

class Strategy(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def onbar(self):

        raise NotImplementedError("应该实现 onbar()!")

#下面提供一个最最简单的例子，“买入并持有”。
class BuyAndHoldStrategy(Strategy):

    '''
    传入bars:DataHandler,events:EventQueue
    '''

    def __init__(self,bars,events,portfolio):

        self.bars = bars
        self.events = events
        self.symbol_list = bars.symbol_list
        self.portfolio = portfolio
        self.bought = {}

        for s in self.symbol_list:
            self.bought[s] = False

    def onbar(self):

        '''
        这个策略很简单，就是判断当前的symbol如果没有买，则产生signalevent,方向是LONG这个symbol。
        也就是未买入就买入并一直持有。
        '''

        for s in self.symbol_list:

            latest_bars = self.bars.get_latest_bars(s,N=1)

            if latest_bars is not None and latest_bars != []:

                if self.bought[s] == False:

                    self.events.put(SignalEvent(s,latest_bars[-1]['date'],'LONG'))
                    #print latest_bars
                    self.bought[s] = True


#下面提供一个最最简单的例子，“买入并持有”。
class BuySpecifiedAndHoldStrategy(Strategy):

    '''
    传入bars:DataHandler,events:EventQueue
    '''

    def __init__(self,bars,events,portfolio):

        self.bars = bars
        self.events = events
        self.symbol_list = bars.symbol_list
        self.portfolio = portfolio
        self.bought = {}

        for s in self.symbol_list:
            self.bought[s] = False

    def onbar(self):

        '''
        这个策略很简单，就是判断当前的symbol如果没有买，则产生signalevent,方向是LONG这个symbol。
        也就是未买入就买入并一直持有。
        '''

        bars = {}

        for sym in self.symbol_list:
            latest_bars = self.bars.get_latest_bars(sym, N=2)
            bars[sym] = latest_bars[-1]['close']

        df_bars = pd.DataFrame.from_dict(bars, orient='index')

        #df_bars = pd.DataFrame.from_dict(list(bars.iteritems()), columns=['close'])
        df_bars = df_bars.sort_values(by=0, ascending=False)
        print df_bars
        buy_list = list(df_bars.head(2).index)

        cash = self.portfolio.current_holdings['cash']
        print 'cash:', cash

        amount = cash / len(buy_list)
        print 'amount:', amount

        for s in self.symbol_list:

            #latest_bars = self.bars.get_latest_bars(s,N=1)
            latest_bars = self.bars.get_latest_bars(s, N=1)
            close = latest_bars[-1]['close']
            quantity = round(amount / close, -2)

            if latest_bars is not None and latest_bars != []:

                if self.bought[s] == True and s not in buy_list:
                    self.events.put(SignalEvent(s, latest_bars[-1]['date'], 'EXIT', quantity))
                    # print latest_bars
                    self.bought[s] = False

        for s in self.symbol_list:

            #latest_bars = self.bars.get_latest_bars(s,N=1)
            latest_bars = self.bars.get_latest_bars(s, N=1)
            close = latest_bars[-1]['close']
            quantity = round(amount / close, -2)

            if latest_bars is not None and latest_bars != []:

                if self.bought[s] == False and s in buy_list:

                    self.events.put(SignalEvent(s,latest_bars[-1]['date'],'LONG', quantity))
                    #print latest_bars
                    self.bought[s] = True


        print 'self.bought:', self.bought

    def calc_quantity(self, symbol):

        pass
