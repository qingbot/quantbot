# -*- coding: utf-8 -*-

import QBUtility
import QBConstant
import QBConfigIO
import QBLoader
import tushare as ts

class QBMonitor(object):

    def __init__(self, df):

        self.__df_data = df
        self.__stock_list = df['code'].values.tolist()
        #self.__stock_list.append('hs300')
        print u'\n>>>QBMonitor: Initialized.'

    def his_price_of_pool_saveto_csv(self):

        for code in self.__stock_list:
            str_code = QBUtility.normalize_code(code)
            stock_pool = QBConstant.DT_STOCK_POOL.keys()

            if str_code not in stock_pool:
                print u'\n>>>QBMonitor: ' + str_code + ' not in POOL.'
                continue

            df_stock_hist_data = ts.get_hist_data(str_code)

            if df_stock_hist_data is not None:
                file_his = QBConfigIO.FOLDER_PATH_HISTORY_PRICE + str_code + '.csv'
                df_stock_hist_data.to_csv(file_his)
                print u'\n>>>QBMonitor: save to ' + file_his

        df_hs300_hist_data = ts.get_hist_data('hs300')
        file_hs300 = QBConfigIO.FOLDER_PATH_HISTORY_PRICE + '000300.csv'
        df_hs300_hist_data.to_csv(file_hs300)
        print u'\n>>>QBMonitor: save to ' + file_hs300

        pass

    def historic_saveto_csv(self):

        i = 0

        for code in self.__stock_list:

            str_code = QBUtility.normalize_code(code)
            df_stock_hist_data = ts.get_hist_data(str_code)

            if df_stock_hist_data is not None:
                file_his = QBConfigIO.FOLDER_PATH_HISTORY_PRICE + str_code + '.csv'
                df_stock_hist_data.to_csv(file_his)
                print u'\n>>>QBMonitor: ' + str(i) + '. save to ' + file_his
                i += 1

        df_hs300_hist_data = ts.get_hist_data('hs300')
        file_hs300 = QBConfigIO.FOLDER_PATH_HISTORY_PRICE + '000300.csv'
        df_hs300_hist_data.to_csv(file_hs300)
        print u'\n>>>QBMonitor: save to ' + file_hs300


if __name__ == '__main__':
    # 股票的历史股价
    obj_qbl_sl = QBLoader.QBLoader(QBConfigIO.FILE_PATH_STOCK_LIST, 1)
    df_sl = obj_qbl_sl.loadto_df()
    obj_qbm = QBMonitor(df_sl)
    obj_qbm.historic_saveto_csv()