# -*- coding: utf-8 -*-

import QBLoader
import QBConfigIO
import pandas as pd
import QBRankComputor

class QBRanking(object):

    def __init__(self):

        print u'\n>>>QBRanking: Initialized.'

    def rank_df(self):
        #读取行业分类
        obj_qbl_in = QBLoader.QBLoader(QBConfigIO.FILE_PATH_INDUSTRY, 1)
        df_in = obj_qbl_in.loadto_df()
        print u'\n>>>QBRanking: industry load to DF.'

        #读取日价格
        obj_qbl_prc = QBLoader.QBLoader(QBConfigIO.FILE_PATH_DAY_PRICE, 1)
        df_prc = obj_qbl_prc.loadto_df()
        #print df.head()
        print u'\n>>>QBRanking: price of day load to DF.'

        #读取东财EPS预测
        obj_qbl_pf = QBLoader.QBLoader(QBConfigIO.FILE_PATH_PROFIT_FORECAST, 1)
        df_stock_forecast = obj_qbl_pf.loadto_df()
        #print df_stock_forecast.head()
        #print type(df_stock_forecast)
        print u'\n>>>QBRanking: procast of profit load to DF.'

        df = pd.merge(df_prc, df_in, on='code')
        df_stock = pd.merge(df_stock_forecast, df, on='code')
        print u'\n>>>QBRanking: merge DFs.'

        obj_qbr = QBRankComputor.QBRankComputor(df_stock)
        obj_qbr.compute_df()
        obj_qbr.saveto_csv()
        obj_qbr.saveto_sql()

class QBRankingIndustry(object):

    def __init__(self):

        print u'\n>>>QBRankingIndustry: Initialized.'

    def rank_df(self):
        #读取行业分类
        obj_qbl_in = QBLoader.QBLoader(QBConfigIO.FILE_PATH_INDUSTRY, 1)
        df_in = obj_qbl_in.loadto_df()
        print u'\n>>>QBRankingIndustry: industry load to DF.'

        # 读取日价格
        obj_qbl_prc = QBLoader.QBLoader(QBConfigIO.FILE_PATH_DAY_PRICE, 1)
        df_prc = obj_qbl_prc.loadto_df()
        # print df.head()
        print u'\n>>>QBRankingIndustry: price of day load to DF.'

        df3 = pd.merge(df_prc, df_in, on='code')
        df3 = df3[df3.pb > 0]
        df3 = df3[df3.per > 0]

        obj_qbri = QBRankComputor.QBRankComputorIndustry(df3)
        obj_qbri.saveto_csv()
        obj_qbri.saveto_sql()

        pass

if __name__ == '__main__':

    obj_qbr = QBRanking()
    obj_qbr.rank_df()

    obj_qbr = QBRankingIndustry()
    obj_qbr.rank_df()

    print u'存储成功'

