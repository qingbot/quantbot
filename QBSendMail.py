# -*- coding: utf-8 -*-

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import QBConfigIO
import QBConstant
import QBLoader
import QBUtility
import time

class QBSendMail(object):

    def __init__(self, subject, content, attachlist=None):

        self.__send_mail_user = QBConstant.MAIL_SEND_USER
        self.__send_mail_pswd = QBConstant.MAIL_SEND_PSWD
        self.__send_mail_host = QBConstant.MAIL_SEND_HOST
        self.__get_mail_user = QBConstant.MAIL_GET_USER
        self.__subject = subject
        self._content = content
        self.__attachlist = attachlist
        print u'\n>>>QBSendMail: Initialized.'

    def send(self):
        # 如名字所示Multipart就是分多个部分
        msg = MIMEMultipart()
        msg["Subject"] = self.__subject
        msg["From"] = self.__send_mail_user
        msg["To"] = self.__get_mail_user

        # ---这是文字部分---

        part = MIMEText(self._content, "html", 'utf-8')
        msg.attach(part)
        print u'\n>>>QBSendMail: Add Content.'


        # ---这是附件部分---
        # xlsx类型附件
        if self.__attachlist:
            for attachfile in self.__attachlist:

                part = MIMEApplication(open(attachfile, 'rb').read())
                part.add_header('Content-Disposition', 'attachment', filename=attachfile)
                msg.attach(part)
                print u'\n>>>QBSendMail: Add Attachment.'
        '''
        # jpg类型附件
        # pdf类型附件
        # mp3类型附件
        '''
        try:
            s = smtplib.SMTP(self.__send_mail_host, timeout=30)  # 连接smtp邮件服务器,端口默认是25
            s.login(self.__send_mail_user, self.__send_mail_pswd)  # 登陆服务器
            s.sendmail(self.__send_mail_user, self.__get_mail_user, msg.as_string())  # 发送邮件
            s.close()
            print u'\n>>>QBSendMail: Send OK.'
            return True
        except Exception, e:
            print str(e)
            return False

    pass


class QBSendMailPool(QBSendMail):

    def __init__(self, subject, content, attachlist=None):
        super(QBSendMailPool, self).__init__(subject, content, attachlist)
        self._content = self.get_content()

    def get_content(self):

        # 周期性股票
        stock_pool = QBConstant.DT_STOCK_POOL
        print stock_pool
        # 生成表头
        mailcontent = '<font size=2>'
        mailcontent += '<table border=1 cellspacing=0 cellpadding=0>'
        mailcontent += '<tr bgcolor=yellow>'
        mailcontent += '<td>code</td>'
        mailcontent += '<td>r10</td>'
        mailcontent += '<td>r30</td>'
        mailcontent += '<td>r60</td>'
        mailcontent += '<td>r90</td>'
        mailcontent += '<td>r120</td>'
        mailcontent += '<td>r180</td>'
        mailcontent += '<td>r240</td>'
        mailcontent += '<td>r300</td>'
        mailcontent += '<td>r360</td>'
        mailcontent += '<td>PE</td>'
        mailcontent += '<td>PB</td>'
        mailcontent += '<td width=100>name</td>'
        mailcontent += '</tr>'

        for key, value in stock_pool.items():
            code = key
            int_code = int(key)
            stock_name = value
            # print code
            print stock_name
            ##df = ts.get_stock_basics()

            obj_qbl_sl= QBLoader.QBLoader(QBConfigIO.FILE_PATH_STOCK_LIST, 1)
            df = obj_qbl_sl.loadto_df()
            print u'\n>>>QBSendMailPool: stock list load to DF.'

            # pe = df[int_code:int_code]['pe'][0]
            pe = df[df.code == int_code]['pe'].values[0]
            pb = df[df.code == int_code]['pb'].values[0]

            file_his = QBConfigIO.FOLDER_PATH_HISTORY_PRICE + code + '.csv'
            obj_qbl_sh = QBLoader.QBLoader(file_his, 1)
            df_stock_h = obj_qbl_sh.loadto_df()
            print u'\n>>>QBSendMailPool: history price load to DF.'


            # print df_stock_h['close'][0]
            # print df_stock_h['close'][30]
            r10 = round((df_stock_h['close'][0] / df_stock_h['close'][10] - 1) * 100, 0)
            r30 = round((df_stock_h['close'][0] / df_stock_h['close'][30] - 1) * 100, 0)
            r60 = round((df_stock_h['close'][0] / df_stock_h['close'][60] - 1) * 100, 0)
            r90 = round((df_stock_h['close'][0] / df_stock_h['close'][90] - 1) * 100, 0)
            r120 = round((df_stock_h['close'][0] / df_stock_h['close'][120] - 1) * 100, 0)
            r180 = round((df_stock_h['close'][0] / df_stock_h['close'][180] - 1) * 100, 0)
            r240 = round((df_stock_h['close'][0] / df_stock_h['close'][240] - 1) * 100, 0)
            r300 = round((df_stock_h['close'][0] / df_stock_h['close'][300] - 1) * 100, 0)
            r360 = round((df_stock_h['close'][0] / df_stock_h['close'][360] - 1) * 100, 0)

            r10_bc = ''
            r30_bc = ''
            r360_bc = ''
            pe_bc = ''
            if r10 > 5:
                r10_bc = 'bgcolor=red'
            elif r10 < -5:
                r10_bc = 'bgcolor=green'

            if r30 > 10:
                r30_bc = 'bgcolor=red'
            elif r30 < -10:
                r30_bc = 'bgcolor=green'

            if r360 > 50:
                r360_bc = 'bgcolor=red'
            elif r360 < -50:
                r360_bc = 'bgcolor=green'

            if pe < 10:
                pe_bc = 'bgcolor=red'
            elif pe > 50:
                pe_bc = 'bgcolor=green'

            print code, ':r30,r60,r90,r120,r180,r240,r300,r360:%s' % r30, r60, r90, r120, r180, r240, r300, r360

            mailcontent += '<tr>'
            mailcontent += '<td>' + code + '</td>'
            mailcontent += '<td ' + r10_bc + '>' + str(r10) + '</td>'
            mailcontent += '<td ' + r30_bc + '>' + str(r30) + '</td>'
            mailcontent += '<td>' + str(r60) + '</td>'
            mailcontent += '<td>' + str(r90) + '</td>'
            mailcontent += '<td>' + str(r120) + '</td>'
            mailcontent += '<td>' + str(r180) + '</td>'
            mailcontent += '<td>' + str(r240) + '</td>'
            mailcontent += '<td>' + str(r300) + '</td>'
            mailcontent += '<td ' + r360_bc + '>' + str(r360) + '</td>'
            mailcontent += '<td ' + pe_bc + '>' + str(pe) + '</td>'
            mailcontent += '<td>' + str(pb) + '</td>'
            mailcontent += '<td>' + stock_name + '</td>'
            mailcontent += '</tr>'
        mailcontent += '</table>'
        mailcontent += '</font>'
        return mailcontent

    def send(self):
        print self._content
        super(QBSendMailPool, self).send()


class QBSendMailMultiFactor(QBSendMail):

    def __init__(self, subject, content, attachlist=None):
        super(QBSendMailMultiFactor, self).__init__(subject, content, attachlist)
        self._content = self.get_content()

    def get_content(self):

        obj_qbl_sl = QBLoader.QBLoader(QBConfigIO.FILE_PATH_STOCK_LIST, 1)
        df = obj_qbl_sl.loadto_df()
        print u'\n>>>QBSendMailMultiFactor: stock list load to DF.'

        df = df[df['pe'] > 0].sort_values(by='pe')
        df['pe_score'] = list(range(1, len(df) + 1))
        # print df.head()

        df = df[df['pb'] > 0].sort_values(by='pb')
        df['pb_score'] = list(range(1, len(df) + 1))
        # print df.head()

        df['score'] = df['pe_score'] + df['pb_score']
        df = df.sort_values(by='score')

        #综合排序，取前30名
        df = df.head(30)

        mailcontent = '<font size=1>'
        mailcontent += '<table border=1 cellspacing=0 cellpadding=0>'
        mailcontent += '<tr bgcolor=yellow>'
        mailcontent += '<td>code</td>'
        mailcontent += '<td>name</td>'
        mailcontent += '<td>industry</td>'
        mailcontent += '<td>pe</td>'
        mailcontent += '<td>pb</td>'
        mailcontent += '<td>pe_score</td>'
        mailcontent += '<td>pb_score</td>'
        mailcontent += '<td>score</td>'
        mailcontent += '</tr>'


        df.index = list(range(len(df)))
        # mailcontent = 'code|name|r30|r60|r90|r120|r180|r240|r300|r360\n'
        print df
        print df[0:1]
        i = 0
        for i in df.index:
            print i
            # print df[i:i+1]

            code = str(df[i:i + 1]['code'].values[0])
            code = QBUtility.normalize_code(code)
            print 'code', code

            stock_name = df[i:i + 1]['name'].values[0]
            indursty = df[i:i + 1]['industry'].values[0]
            pe = df[i:i + 1]['pe'].values[0]
            print 'type-pe', type(pe)
            pe = str(pe)
            pb = str(df[i:i + 1]['pb'].values[0])
            pe_score = str(df[i:i + 1]['pe_score'].values[0])
            pb_score = str(df[i:i + 1]['pb_score'].values[0])
            score = str(df[i:i + 1]['score'].values[0])
            # print code
            # print stock_name
            print code, ':name,pe,pb,pe_score,pb_score,score:%s' % stock_name, pe, pb, pe_score, pb_score, score

            mailcontent += '<tr>'
            mailcontent += '<td>' + code + '</td>'
            mailcontent += '<td>' + stock_name + '</td>'
            mailcontent += '<td>' + indursty + '</td>'
            mailcontent += '<td>' + pe + '</td>'
            mailcontent += '<td>' + pb + '</td>'
            mailcontent += '<td>' + pe_score + '</td>'
            mailcontent += '<td>' + pb_score + '</td>'
            mailcontent += '<td>' + score + '</td>'
            mailcontent += '</tr>'

        mailcontent += '</table></font>'
        #mailcontent = mailcontent.decode('UTF-8')

        return mailcontent

    def send(self):
        print self._content
        super(QBSendMailMultiFactor, self).send()


if __name__ == '__main__':
    #obj_qbsm = QBSendMail(QBConstant.MAIL_SUBJECT_ATTACH, QBConstant.MAIL_SUBJECT_ATTACH)
    #obj_qbsm.send()
    #obj_qbsm = QBSendMail(QBConstant.MAIL_SUBJECT_ATTACH, QBConstant.MAIL_SUBJECT_ATTACH, QBConfigIO.FILE_LIST_MAIL)
    #obj_qbsm.send()

    obj_qbsp = QBSendMailPool(QBConstant.MAIL_SUBJECT_ATTACH, QBConstant.MAIL_SUBJECT_ATTACH, QBConfigIO.FILE_LIST_MAIL)
    obj_qbsp.send()

    obj_qbsm = QBSendMailMultiFactor(QBConstant.MAIL_SUBJECT_MULTIFACTOR, QBConstant.MAIL_SUBJECT_MULTIFACTOR)
    obj_qbsm.send()