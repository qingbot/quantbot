# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import pandas as pd
import requests
import csv
from time import sleep
import scipy as sp
from scipy.stats import poisson

import QBLoader

class QBWorldCupHistoric(object):
    def __init__(self):
        self._team_dict = {
            #A组
            735: '埃及',
            746: '俄罗斯',
            767: '乌拉圭',
            891: '沙特阿拉伯',
            #B组
            765: '葡萄牙',
            772: '西班牙',
            783: '伊朗',
            813: '摩洛哥',
            #c组
            638: '丹麦',
            649: '法国',
            774: '秘鲁',
            913: '澳大利亚',
            #D组
            756: '冰岛',
            766: '阿根廷',
            768: '克罗地亚',
            789: '尼日利亚',
            #E组
            642: '塞尔维亚',
            648: '瑞士',
            778: '巴西',
            914: '哥斯达黎加',
            #F组
            644: '瑞典',
            650: '德国',
            819: '墨西哥',
            898: '韩国',
            #G组
            645: '比利时',
            744: '英格兰',
            798: '巴拿马',
            823: '突尼斯',
            #H组
            637: '波兰',
            775: '哥伦比亚',
            815: '塞内加尔',
            903: '日本'
        }


        self._team_list = [[735, 746, 767, 891],
                         [765, 772, 873, 813],
                         [638, 649, 774, 913],
                         [756, 766, 768, 789],
                         [642, 648, 778, 914],
                         [644, 650, 819, 898],
                         [645, 744, 798, 823],
                         [637, 775, 815, 903]
                         ]

        self.__game_info = pd.DataFrame()
        self.__host_strength = pd.DataFrame()
        self.__guest_strength = pd.DataFrame()
        self.__host_strength_by_no = pd.DataFrame()
        self.__guest_strength_by_no = pd.DataFrame()
        self.__team_strength =pd.DataFrame()


    def get_df_team_dict(self):

        df = pd.DataFrame.from_dict(self._team_dict, orient='index')
        df = df.rename(columns={0:'team_name'})
        # print df
        return df

    def downloader_team_info(self):

        for team_id in self._team_dict.keys():


            print 'team_id:', team_id
            pagenum = 11

            i = 1

            for i in range(1, pagenum):

                url = 'http://zq.win007.com/cn/team/TeamScheAjax.aspx?TeamID={}'.format(team_id)
                url += '&pageNo={}'.format(i)
                #url += '&flesh=0.07528568459088403'
                print url

                #url = 'http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=C._A&sty=GEMCPF&st=(AllNum)&sr=-1&p={}'.format(i)
                #url = url +'&ps=50&cb=&js=var%20YBIYlkKH={%22data%22:[(x)],%22pages%22:%22(pc)%22}&token=3a965a43f705cf1d9ad7e1a3e429d622&rt=49511416'

                headers = {
                    'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36'
                    }

                try:
                    wb_data = requests.get(url, headers=headers)

                except requests.exceptions.ConnectionError:
                    print 'ConnectionError'

                else:
                    if wb_data is None:
                        continue
                    print wb_data
                    soup = BeautifulSoup(wb_data.text, 'lxml')
                    body = soup.body.text
                    print body
                    sleep(1)
                    body1 = body.split('=')[2]
                    print body1[2]

                    body2 = body1.split('],')
                    print len(body2)
                    body2_len = len(body2)

                    filename = 'QBData\\WorldCup\\' + str(team_id) + '.csv'

                    with open(filename, 'ab') as csvfile:

                        spamwriter = csv.writer(csvfile, dialect='excel')

                        for j in range(0, body2_len):

                            #print body2[j]
                            #body2_line = body2[j].replace('\'', '').replace('[', '').encode('GBK')
                            body2_line = body2[j].replace('\'', '').replace('[', '').replace('\r', '').encode('GBK')
                            print body2_line

                            body2_line_list = body2_line.split(',')
                            spamwriter.writerow(body2_line_list)
                            print u'[文件]第', j, u'数据写入完毕'


                            pass

                    sleep(0.5)

    def downloader_single_page_info(self, team_id, i):

        filename = 'QBData\\WorldCup\\' + str(team_id) + '_test.csv'

        print u'[启动]打开文件：', filename

        with open(filename, 'wb') as csvfile:

            spamwriter = csv.writer(csvfile, dialect='excel')
            csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score', 'game_name1', 'game_name2',
                          'game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1', 'guest_name2', 'guest_name3', 'a', 'b', 'c',
                          'bet_result1', 'bet_result2', 'bet_result3', 'bet_result4']
            print u'[文件]写入表头'
            spamwriter.writerow(csv_header)

        url = 'http://zq.win007.com/cn/team/TeamScheAjax.aspx?TeamID={}'.format(team_id)
        url += '&pageNo={}'.format(i)
        # url += '&flesh=0.07528568459088403'
        print url

        # url = 'http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=C._A&sty=GEMCPF&st=(AllNum)&sr=-1&p={}'.format(i)
        # url = url +'&ps=50&cb=&js=var%20YBIYlkKH={%22data%22:[(x)],%22pages%22:%22(pc)%22}&token=3a965a43f705cf1d9ad7e1a3e429d622&rt=49511416'

        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36'
        }

        try:
            wb_data = requests.get(url, headers=headers)

        except requests.exceptions.ConnectionError:
            print 'ConnectionError'

        else:
            if wb_data is None:
                return

            print wb_data
            soup = BeautifulSoup(wb_data.text, 'lxml')
            body = soup.body.text
            print body
            sleep(1)
            body1 = body.split('=')[2]
            print body1

            body2 = body1.split('],')
            print len(body2)
            body2_len = len(body2)

            with open(filename, 'ab') as csvfile:

                spamwriter = csv.writer(csvfile, dialect='excel')

                for j in range(0, body2_len):
                    print 'body2[j]:', body2[j]
                    # body2_line = body2[j].replace('\'', '').replace('[', '').encode('GBK')
                    body2_line = body2[j].replace('\'', '').replace('[', '').replace('\n', '').replace(']','').replace(' ','').replace(';','').encode('GBK')
                    print 'body2_line:', body2_line

                    body2_line_list = body2_line.split(',')
                    spamwriter.writerow(body2_line_list)
                    print u'[文件]第', j, u'数据写入完毕'

                    pass

            sleep(0.5)


    def downloader_single_team_info(self, team_id):

        filename = 'QBData\\WorldCup\\' + str(team_id) + '_test.csv'

        print u'[启动]打开文件：', filename

        with open(filename, 'wb') as csvfile:

            spamwriter = csv.writer(csvfile, dialect='excel')
            csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score', 'game_name1', 'game_name2',
                          'game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1', 'guest_name2', 'guest_name3', 'a', 'b', 'c',
                          'bet_result1', 'bet_result2', 'bet_result3', 'bet_result4']
            print u'[文件]写入表头'
            spamwriter.writerow(csv_header)

        i = 1
        pagenum = 11

        for i in range(1, pagenum):

            url = 'http://zq.win007.com/cn/team/TeamScheAjax.aspx?TeamID={}'.format(team_id)
            url += '&pageNo={}'.format(i)
            # url += '&flesh=0.07528568459088403'
            print url

            # url = 'http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=C._A&sty=GEMCPF&st=(AllNum)&sr=-1&p={}'.format(i)
            # url = url +'&ps=50&cb=&js=var%20YBIYlkKH={%22data%22:[(x)],%22pages%22:%22(pc)%22}&token=3a965a43f705cf1d9ad7e1a3e429d622&rt=49511416'

            headers = {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36'
            }

            try:
                wb_data = requests.get(url, headers=headers)

            except requests.exceptions.ConnectionError:
                print 'ConnectionError'

            else:
                if wb_data is None:
                    return

                print wb_data
                soup = BeautifulSoup(wb_data.text, 'lxml')
                body = soup.body.text
                print body
                sleep(1)
                body1 = body.split('=')[2]
                print body1

                body2 = body1.split('],')
                print len(body2)
                body2_len = len(body2)

                with open(filename, 'ab') as csvfile:

                    spamwriter = csv.writer(csvfile, dialect='excel')

                    for j in range(0, body2_len):
                        print 'body2[j]:', body2[j]
                        # body2_line = body2[j].replace('\'', '').replace('[', '').encode('GBK')
                        body2_line = body2[j].replace('\'', '').replace('[', '').replace('\n', '').replace(']','').replace(' ','').replace(';','').encode('GBK')
                        print 'body2_line:', body2_line
                        body2_line = body2_line.replace('\r', '')
                        body2_line = body2_line.replace('\0', '')

                        body2_line_list = body2_line.split(',')
                        spamwriter.writerow(body2_line_list)
                        print u'[文件]第', j, u'数据写入完毕'

                        pass

                sleep(0.5)


    def get_df_single_page_info(self, team_id, i):

        url = 'http://zq.win007.com/cn/team/TeamScheAjax.aspx?TeamID={}'.format(team_id)
        url += '&pageNo={}'.format(i)
        # url += '&flesh=0.07528568459088403'
        print url

        # url = 'http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=C._A&sty=GEMCPF&st=(AllNum)&sr=-1&p={}'.format(i)
        # url = url +'&ps=50&cb=&js=var%20YBIYlkKH={%22data%22:[(x)],%22pages%22:%22(pc)%22}&token=3a965a43f705cf1d9ad7e1a3e429d622&rt=49511416'

        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36'
        }

        data = []

        try:
            wb_data = requests.get(url, headers=headers)

        except requests.exceptions.ConnectionError:
            print 'ConnectionError'

        else:
            if wb_data is None:
                return

            print wb_data
            soup = BeautifulSoup(wb_data.text, 'lxml')
            body = soup.body.text
            print body
            sleep(1)
            body1 = body.split('=')[2]
            print body1

            body2 = body1.split('],')
            print len(body2)
            body2_len = len(body2)



            for j in range(0, body2_len):
                print 'body2[j]:', body2[j]
                # body2_line = body2[j].replace('\'', '').replace('[', '').encode('GBK')
                body2_line = body2[j].replace('\'', '').replace('[', '').replace('\n', '').replace(']','').replace(' ','').replace(';','').encode('GBK')
                print 'body2_line:', body2_line

                body2_line_list = body2_line.split(',')

                data.append(body2_line_list)
                print u'[文件]第', j, u'数据写入完毕'

                pass

            sleep(0.5)

        csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score',
                      'game_name1', 'game_name2',
                      'game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1', 'guest_name2', 'guest_name3',
                      'a', 'b', 'c',
                      'bet_result1', 'bet_result2', 'bet_result3', 'bet_result4']

        df = pd.DataFrame(data, columns=csv_header)

        filename = 'QBData\\WorldCup\\' + str(team_id) + '_test.csv'

        df.to_csv(filename)

    def get_df_single_team_info(self, team_id):
        i = 1
        pagenum = 11
        data = []

        for i in range(1, pagenum):

            url = 'http://zq.win007.com/cn/team/TeamScheAjax.aspx?TeamID={}'.format(team_id)
            url += '&pageNo={}'.format(i)
            # url += '&flesh=0.07528568459088403'
            print url

            # url = 'http://nufm.dfcfw.com/EM_Finance2014NumericApplication/JS.aspx?type=CT&cmd=C._A&sty=GEMCPF&st=(AllNum)&sr=-1&p={}'.format(i)
            # url = url +'&ps=50&cb=&js=var%20YBIYlkKH={%22data%22:[(x)],%22pages%22:%22(pc)%22}&token=3a965a43f705cf1d9ad7e1a3e429d622&rt=49511416'

            headers = {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36'
            }

            try:
                wb_data = requests.get(url, headers=headers)

            except requests.exceptions.ConnectionError:
                print 'ConnectionError'

            else:
                if wb_data is None:
                    return

                print wb_data
                soup = BeautifulSoup(wb_data.text, 'lxml')
                body = soup.body.text
                print body
                sleep(1)
                body1 = body.split('=')[2]
                print body1

                body2 = body1.split('],')
                print len(body2)
                body2_len = len(body2)



                for j in range(0, body2_len):
                    print 'body2[j]:', body2[j]
                    # body2_line = body2[j].replace('\'', '').replace('[', '').encode('GBK')
                    body2_line = body2[j].replace('\'', '').replace('[', '').replace('\n', '').replace(']','').replace(' ','').replace(';','').encode('GBK')
                    print 'body2_line:', body2_line

                    body2_line_list = body2_line.split(',')

                    data.append(body2_line_list)
                    print u'[文件]第', j, u'数据写入完毕'

                    pass

                sleep(0.5)

        csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score',
                      'game_name1', 'game_name2',
                      'game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1', 'guest_name2', 'guest_name3',
                      'a', 'b', 'c',
                      'bet_result1', 'bet_result2', 'bet_result3', 'bet_result4', 'bet_result5', 'bet_result6']

        df = pd.DataFrame(data, columns=csv_header)
        #df = pd.DataFrame(data)

        filename = 'QBData\\WorldCup\\' + str(team_id) + '_test.csv'

        df.to_csv(filename)

    def get_df_team_info(self):

        for team_id in self._team_dict.keys():

            print 'team_id:', team_id
            pagenum = 11
            data = []
            lens_info = 0

            for i in range(1, pagenum):

                url = 'http://zq.win007.com/cn/team/TeamScheAjax.aspx?TeamID={}'.format(team_id)
                url += '&pageNo={}'.format(i)
                # url += '&flesh=0.07528568459088403'
                print url

                headers = {
                    'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Mobile Safari/537.36'
                }

                try:
                    wb_data = requests.get(url, headers=headers)

                except requests.exceptions.ConnectionError:
                    print 'ConnectionError'

                else:
                    if wb_data is None:
                        return

                    print wb_data
                    soup = BeautifulSoup(wb_data.text, 'lxml')
                    body = soup.body.text
                    print body
                    sleep(1)
                    body1 = body.split('=')[2]
                    print body1

                    body2 = body1.split('],')
                    print len(body2)
                    body2_len = len(body2)

                    for j in range(0, body2_len):
                        print 'body2[j]:', body2[j]
                        # body2_line = body2[j].replace('\'', '').replace('[', '').encode('GBK')
                        body2_line = body2[j].replace('\'', '').replace('[', '').replace('\n', '').replace(']','').replace(' ','').replace(';','').encode('GBK')
                        print 'body2_line:', body2_line

                        body2_line_list = body2_line.split(',')

                        data.append(body2_line_list)
                        print u'[文件]第', j, u'数据写入完毕'

                        if len(body2_line_list) > lens_info:

                            lens_info = len(body2_line_list)

                        pass

                    sleep(0.5)

            if lens_info == 26:
                csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score',
                              'game_name1', 'game_name2','game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1',
                              'guest_name2', 'guest_name3','a', 'b', 'c','bet_result1', 'bet_result2', 'bet_result3',
                              'bet_result4', 'bet_result5', 'bet_result6']
            elif lens_info ==25:
                csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score',
                              'game_name1', 'game_name2',
                              'game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1', 'guest_name2',
                              'guest_name3',
                              'a', 'b', 'c',
                              'bet_result1', 'bet_result2', 'bet_result3', 'bet_result4', 'bet_result5']
            else:
                csv_header = ['game_no1', 'game_no2', 'game_no3', 'game_date', 'host_no', 'guest_no', 'score', 'half_score',
                              'game_name1', 'game_name2',
                              'game_name3', 'host_name1', 'host_name2', 'host_name3', 'guest_name1', 'guest_name2',
                              'guest_name3',
                              'a', 'b', 'c',
                              'bet_result1', 'bet_result2', 'bet_result3', 'bet_result4']

            df = pd.DataFrame(data, columns=csv_header)
            #df = pd.DataFrame(data)

            filename = 'QBData\\WorldCup\\' + str(team_id) + '.csv'

            df.to_csv(filename)

    def run(self, team_id1 = 735):

        filename = 'QBData\\WorldCup\\' + str(team_id1) + '.csv'

        #df_team = pd.read_csv(filename)
        df_team = pd.read_csv(filename, encoding='GBK')

        print df_team
        return df_team

    def calc_game_info(self):

        df_team_all = pd.DataFrame()

        for team_id in self._team_dict.keys():

            filename = 'QBData\\WorldCup\\' + str(team_id) + '.csv'
            print filename

            # df_team = pd.read_csv(filename)
            df_team = pd.read_csv(filename, encoding='GBK')
            #print df_team
            frames = [df_team_all, df_team]

            # df_team_all = pd.concat(frames, ignore_index=True)
            df_team_all = pd.concat(frames)
            # df_team_all.append(df_team)

        df_new = df_team_all.score.str.split('-', expand=True)

        # print df_new
        # df_new.to_csv('QBData\\WorldCup\\split.csv', encoding='GBK')
        # print df_new[0]
        # print df_new[1]
        df_team_all['host_score'] = df_new[0]
        df_team_all['guest_score'] = df_new[1]

        print df_team_all

        df_team_all.to_csv('QBData\\WorldCup\\WorldCupAll.csv', encoding='GBK')

        self.__game_info = df_team_all

    def calc_team_strength(self):

        if self.__game_info.empty:
            self.__game_info = pd.read_csv('QBData\\WorldCup\\WorldCupAll.csv', encoding='GBK')

        col = ['game_date', 'host_no', 'host_name1', 'guest_no', 'guest_name1', 'host_score', 'guest_score']
        game_info = self.__game_info.loc[:, col]

        game_info = game_info.dropna(axis=0, how='any')

        game_info['host_score'] = game_info['host_score'].astype(int)
        game_info['guest_score'] = game_info['guest_score'].astype(int)

        print '--------------------------'
        # print game_info

        host_strength_avg = game_info.host_score.mean()
        guest_strength_avg = game_info.guest_score.mean()

        # print type(host_strength_avg), host_strength_avg
        # print type(guest_strength_avg), guest_strength_avg

        # 进攻实力

        host_attack_score = game_info[['host_score']].groupby(game_info['host_no']).sum()
        host_attack_count = game_info[['host_score']].groupby(game_info['host_no']).count()
        host_attack_score['host_count'] = host_attack_count['host_score']
        #print host_attack_score

        guest_attack_score = game_info[['guest_score']].groupby(game_info['guest_no']).sum()
        guest_attack_count = game_info[['guest_score']].groupby(game_info['guest_no']).count()
        guest_attack_score['guest_count'] = guest_attack_count['guest_score']
        #print guest_attack_score

        attack_score = pd.merge(host_attack_score, guest_attack_score, left_index=True, right_index=True)
        attack_score['attack_strength'] = (attack_score['host_score'] + attack_score['guest_score']) / (attack_score['host_count'] + attack_score['guest_count'])
        attack_score['attack_strength'] = attack_score['attack_strength'] / host_strength_avg
        # print attack_score

        # 防守实力
        host_defense_score = game_info[['guest_score']].groupby(game_info['host_no']).sum()
        host_defense_count = game_info[['guest_score']].groupby(game_info['host_no']).count()
        host_defense_score['guest_count'] = host_defense_count['guest_score']
        #print host_defense_score

        guest_defense_score = game_info[['host_score']].groupby(game_info['guest_no']).sum()
        guest_defense_count = game_info[['host_score']].groupby(game_info['guest_no']).count()
        guest_defense_score['host_count'] = guest_defense_count['host_score']
        #print guest_defense_score

        defense_score = pd.merge(host_defense_score, guest_defense_score, left_index=True, right_index=True)
        defense_score['defense_strength'] = (defense_score['host_score'] + defense_score['guest_score']) / (
                defense_score['host_count'] + defense_score['guest_count'])
        defense_score['defense_strength'] = defense_score['defense_strength'] / guest_strength_avg
        # print defense_score


        '''
        host_strength = game_info[['host_score']].groupby(game_info['host_name1']).mean() / host_strength_avg
        host_strength_by_no = game_info[['host_score']].groupby(game_info['host_no']).mean() / host_strength_avg
        

        # 防守实力
        guest_strength = game_info[['guest_score']].groupby(game_info['guest_name1']).mean() / guest_strength_avg
        guest_strength_by_no = game_info[['guest_score']].groupby(game_info['guest_no']).mean() / guest_strength_avg

        #print host_strength
        #print guest_strength
        '''

        self.__host_strength = attack_score.loc[:, ['attack_strength']]
        self.__guest_strength = defense_score.loc[:, ['defense_strength']]

        # host_strength_by_no = host_strength_by_no.rename(index={"host_no":"team_no"})
        # guest_strength_by_no = guest_strength_by_no.rename(index={"guest_no":"team_no"})

        # print 'host_strength_by_no:\n', host_strength_by_no
        # print 'guest_strength_by_no:\n', guest_strength_by_no

        # team_strength_by_no = pd.merge(host_strength_by_no, guest_strength_by_no, left_index=True, right_index=True)
        # print team_strength_by_no

        team_strength = pd.merge(self.__host_strength, self.__guest_strength, left_index=True, right_index=True)
        team_strength.to_csv('QBData\\WorldCup\\team_strength.csv', encoding='GBK')

        # self.__host_strength_by_no = host_strength_by_no
        # self.__guest_strength_by_no = guest_strength_by_no
        self.__team_strength = team_strength
        pass

    def simulate_match(self, team_A, team_B, knockout=False):
        """模拟一场比赛，返回主队进球数、客队进球数"""
        team_strength = self.__team_strength

        print 'team_A, host_score:', team_strength.loc[team_A, 'attack_strength']
        print 'team_B, guest_score:', team_strength.loc[team_B, 'defense_strength']
        print 'team_A, guest_score:', team_strength.loc[team_A, 'defense_strength']
        print 'team_B host_score:', team_strength.loc[team_B, 'attack_strength']

        # 获取比赛双方进球率、失球率
        home_scoring_strength = (team_strength.loc[team_A, 'attack_strength'] + team_strength.loc[team_B, 'defense_strength']) / 2
        away_scoring_strength = (team_strength.loc[team_A, 'defense_strength'] + team_strength.loc[team_B, 'attack_strength']) / 2

        n_sim = 500
        # 模拟n次比赛进球数取众数
        fs_A = sp.stats.mode(poisson.rvs(home_scoring_strength, size=n_sim))[0][0]
        fs_B = sp.stats.mode(poisson.rvs(away_scoring_strength, size=n_sim))[0][0]
        print(team_A, fs_A, team_B, fs_B)

        # 进入淘汰赛，若平局，点球大战晋级概率50%：50%
        if knockout:
            if fs_A == fs_B:
                return [team_A, team_B][sp.random.randint(0, 2)]
            elif fs_A > fs_B:
                return team_A
            else:
                return team_B

        print 'fs_A:', self._team_dict[team_A], fs_A
        print 'fs_B:', self._team_dict[team_B], fs_B
        return fs_A, fs_B


class Group:
    """模拟小组赛阶段，直接调用.play方法。"""

    def __init__(self, historic, group_teams, group_name, fixture):

        self.historic = historic

        self.group_teams = group_teams
        self.group_name = group_name

        self.table = pd.DataFrame(0, columns=['场次', '积分', '进球', '失球', '净胜球'], index=self.group_teams)
        self.fixture = fixture
        self.result = None

    def play(self):
        result = []
        for [team_A, team_B] in self.fixture:

            fs_A, fs_B = self.historic.simulate_match(team_A, team_B)
            self.table.loc[team_A, '场次'] += 1
            self.table.loc[team_B, '场次'] += 1
            self.table.loc[team_A, '进球'] += fs_A
            self.table.loc[team_B, '进球'] += fs_B
            self.table.loc[team_A, '失球'] += fs_B
            self.table.loc[team_B, '失球'] += fs_A
            if fs_A > fs_B:
                self.table.loc[team_A, '积分'] += 3
            elif fs_A == fs_B:
                self.table.loc[team_A, '积分'] += 1
                self.table.loc[team_B, '积分'] += 1
            elif fs_A < fs_B:
                self.table.loc[team_B, '积分'] += 1
            else:
                raise ValueError('比赛比分模拟有误！')
            result.append([team_A, team_B, fs_A, fs_B])
        self.result = pd.DataFrame(result, columns=['主队', '客队', '主队进球', '客队进球'])
        self.table['净胜球'] = self.table['进球'] - self.table['失球']
        self.table.sort_values(by=['积分', '净胜球', '进球'],
                               ascending=[False, False, False], inplace=True)

        team_name = self.historic.get_df_team_dict()

        # self.result = pd.merge(self.result, team_name, left_index=True, right_index=True)
        self.table = pd.merge(self.table, team_name, left_index=True, right_index=True)
        print self.result
        print self.table

wc = QBWorldCupHistoric()
# wc.get_df_team_info()
# wc.get_df_single_page_info(735, 1)
# wc.get_df_single_team_info(735)

# wc.downloader_single_page_info(735, 1)
# wc.downloader_single_team_info(735)
# wc.downloader_team_info()

'''
df = wc.run()
df_new = df.score.str.split('-', expand=True)
print df_new
df_new.to_csv('QBData\\WorldCup\\split.csv', encoding='GBK')
print df_new[0]
print df_new[1]
df['host_score'] = df_new[0]
df['guest_score'] = df_new[1]
df.fillna(0)

print df

'''
# wc.calc_game_info()
wc.calc_team_strength()
#wc.simulate_match(898, 650)
wc.get_df_team_dict()

fixture_A = [[746, 891],
             [735, 767],
             [746, 735],
             [767, 891],
             [891, 735],
             [746, 767]
             ]
group_a_teams = [735, 746, 767, 891]

group_a = Group(wc, group_a_teams, group_a_teams, fixture_A)
group_a.play()



