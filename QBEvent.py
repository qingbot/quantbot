## -*- coding: utf-8 -*-


'''
上篇文章实现了量化回测系统的基础框架，本文继续。这篇文章，主要介绍事件Event类型，它如何在各个组件之间传递信息。

上篇文章Backtest里的两层循环里，内层循环是对事件的处理。事件类型如下述4种：
  ● BarEvent - 新的tick，也就是新的Bar（可以认为就是一个K线，这里简单起见就用日线，换成5分钟线，小时线类似）到达。
    当DataHandler.update_ars更新时触发，用于Strategy计算交易信号，Portfolio更新仓位信息。BarEvent只需要一个type，没有其他的成员变量。
  ● SignalEvent -信号事件。Strategy在处理每天的Bar时，如果按模型计算，需要产生信号时，触发Signal事件，包含对特定symbol做多，做空或平仓。
    SignalEvent被Porfolio对象用于计算如何交易。
  ● OrderEvent - 订单事件。当Porfolio对象接受一件SignalEvent事件时，会根据当前的风险和仓位，发现OrderEvent给ExecutionHandler执行器。
  ● FillEvent - 交易订单。当执行器ExecutionHandler接收到OrderEvent后就会执行交易订单，订单交易完成时会产生FillEvent，给Porfolio对象去更新成本，仓位情况等操作。

'''
class Event(object):
    pass


class BarEvent(Event):

    def __init__(self):
        self.type = 'BAR'

class SignalEvent(Event):

    def __init__(self, symbol, datetime, signal_type, quantity):

        self.type = 'SIGNAL'
        self.symbol = symbol
        self.datetime = datetime
        self.signal_type = signal_type
        self.quantity = quantity

class OrderEvent(Event):

    def __init__(self, symbol, order_type, quantity,direction):

        self.type = 'ORDER'
        self.symbol = symbol
        self.order_type = order_type
        self.direction = direction
        self.quantity = quantity

    def print_order(self):

        print("Order: Symbol=%s, Type=%s, Quantity=%s, Direction=%s" %
              (self.symbol, self.order_type, self.direction))

class FillEvent(Event):

    def __init__(self, timeindex, symbol,quantity, direction, fill_price):

        self.type = 'FILL'
        self.timeindex = timeindex
        self.symbol = symbol
        self.quantity = quantity
        self.direction = direction
        self.fill_price = fill_price