# -*- coding: utf-8 -*-


import datetime
import numpy as np
import pandas as pd
import queue
from QBEvent import FillEvent,OrderEvent
from QBPerformance import create_drawdowns,create_sharpe_ratio
import QBConfigIO
#from aiquant.engine.benchmark import Benchmark
import matplotlib.pyplot as plt
#%matplotlib inline

from abc import ABCMeta, abstractmethod
from math import floor

class Portfolio(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def on_signal(self,event):

        raise NotImplementedError("Should implement on_signal()")

    @abstractmethod
    def on_fill(self,event):

        raise NotImplementedError("Should implement on_fill()")

'''
下面是具体SimplePortfolio实现，默认初始资金设定为100000，传入数据处理器和事件队列。
all_positions是一个每天仓位的数组，current_positions是一个dict，保存最近一次组合里的仓位情况，即｛symbol1:100,symbol2:300,...｝。
benchbark是策略的对比基准，比如是沪深300指数或上证综合指数。all_hodings是保存每天组合市值的信息，current_holdings是最近一次的holding信息，是一个dict。
'''

class SimplePortfolio(Portfolio):

    def __init__(self, bars, events, initial_capital=1500000.0):

        self.bars = bars
        self.events = events
        self.symbol_list = self.bars.symbol_list
        self.initial_capital = initial_capital
        self.commission = 0.0

        self.all_buy_list = []
        self.current_buy_list = []

        self.all_positions = []
        #初始化所有的持仓为0
        self.current_positions = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])

        self.all_holdings = []
        self.current_holdings = self.construct_current_holdings()


        #self.benchmark = Benchmark(bars,initial_capital)


        #当天的holding包含总体组合目前cash（现金）,commission（交易费用）,total（组合净资产），还包含每个持仓Symbol的现值。

    def construct_current_holdings(self):
        """
        当前也就是初始的组合（证券+现金）情况。d是一个dict类
        d:｛symbole1:0.0,cash:init,commission:0.0,total:init｝,就是没有持有任何证券
        total就是初始资金
        """

        d = dict((k, v) for k, v in [(s, 0.0) for s in self.symbol_list])
        d['cash'] = self.initial_capital
        d['commission'] = 0.0
        d['total'] = self.initial_capital

        return d

        # 实现基类的on_signal，接收到SignalEvent，就直接产生订单。
        # 这里只实现"市价单"，LONG且之前未持仓，则买入100股； SHORT且未持仓，则卖出100股，EXIT平仓则有持仓就清仓。
        # 对于下单事件，就只有BUY/SELL两个方向。

    '''
    Strategy里会产生SignalEvent，Portfolio会接收并处理
    '''

    def on_signal(self, event):
        order_event = self.generate_order_event(event)

        self.events.put(order_event)

    #这里为了演示，直接产生一个确定的股数。

    def generate_order_event(self, signal_event):

        order = None
        symbol = signal_event.symbol
        direction = signal_event.signal_type

        #mkt_quantity = floor(5000)
        mkt_quantity = signal_event.quantity
        # mkt_quantity = self._calc_quantity(symbol)

        cur_quantity = self.current_positions[symbol]
        order_type = 'MKT'

        # 这里只实现"市价单"，LONG且之前未持仓，则买入100股； SHORT且未持仓，则卖出100股，EXIT平仓则有持仓，则清仓
        # 对于下单事件，就只有BUY/SELL

        if direction == 'LONG' and cur_quantity == 0:
            order = OrderEvent(symbol, order_type, mkt_quantity, 'BUY')

        if direction == 'SHORT' and cur_quantity == 0:
            order = OrderEvent(symbol, order_type, mkt_quantity, 'SELL')

        if direction == 'EXIT' and cur_quantity > 0:
            order = OrderEvent(symbol, order_type, abs(cur_quantity), 'SELL')

        if direction == 'EXIT' and cur_quantity < 0:
            order = OrderEvent(symbol, order_type, abs(cur_quantity), 'BUY')


        return order

    # 订单发送到交易所，交易所处理后返回处理结果，会产生fill事件，on_fill里处理fill事件，更新position和holding。

    def on_fill(self, event):

        if event.type == 'FILL':
            self.update_positions_from_fill(event)
            self.update_holdings_from_fill(event)

    # 当前的position在本次fill之后变化了，更新到current_positions里

    def update_positions_from_fill(self, fill):

        # fill的方向，只有BUY/SELL,+/-quantity

        fill_dir = 0

        if fill.direction == 'BUY':
            fill_dir = 1

        if fill.direction == 'SELL':
            fill_dir = -1

        # 把当前的symbol的持仓更新
        self.current_positions[fill.symbol] += fill_dir * fill.quantity

    #当前的holdings在本次fill之后变化了，更新到current_holdings里，因为有交易费用情况，组合的净值也会生了变化。

    def update_holdings_from_fill(self, fill):

        fill_dir = 0

        if fill.direction == 'BUY':
            fill_dir = 1

        if fill.direction == 'SELL':
            fill_dir = -1

        # Update holdings list with new quantities

        fill_cost = self.bars.get_latest_bars(fill.symbol)[0]['close']  # 收盘价
        cost = fill_dir * fill_cost * fill.quantity

        # 每个symbol的持续成本分别保存

        self.current_holdings[fill.symbol] += cost  # 持仓成本/市值
        self.current_holdings['commission'] += self.commission
        self.current_holdings['cash'] -= (cost + self.commission)  # 现金
        self.current_holdings['total'] -= (self.commission)  # 组合总价值

    #另外还有一件很重要的事情，我们的all_positions, all_holding现在还没有数据。
    # 在每天收市，即onbar事件到达时，我们update_timeindex去更新每天的仓位及组合净值情况。

    def update_timeindex(self):

        """
       新的市场数据到达，这是一个BarEvent
        """

        # benchmark也更新数据
        # self.benchmark.onbar()

        bars = {}

        for sym in self.symbol_list:
            bars[sym] = self.bars.get_latest_bars(sym, N=1)

        # Update positions
        dp = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])

        dp['date'] = bars[self.symbol_list[0]][-1]['date']

        for s in self.symbol_list:
            dp[s] = self.current_positions[s]

        # 加一个current_positions

        self.all_positions.append(dp)

        # 更新组合资产情况

        dh = dict((k, v) for k, v in [(s, 0) for s in self.symbol_list])
        dh['date'] = bars[self.symbol_list[0]][-1]['date']
        dh['cash'] = self.current_holdings['cash']
        dh['commission'] = self.current_holdings['commission']
        dh['total'] = self.current_holdings['cash']

        for s in self.symbol_list:
            # Approximation to the real value

            market_value = self.current_positions[s] * bars[s][0]['close']
            dh[s] = market_value
            dh['total'] += market_value

        # 每天的情况保存下来
        self.all_holdings.append(dh)


    def calc_performance(self):

        df = pd.DataFrame(self.all_holdings)
        df.set_index('date', inplace=True)
        df['returns'] = df['total'].pct_change()  # 总资产的变化情况就是收益率
        df['equity_curve'] = (1.0 + df['returns']).cumprod()
        df_banchmark = self.benchmark.calc_performance()
        all_performance = pd.merge(df_banchmark, df, left_index=True, right_index=True)
        all_performance['date'] = all_performance.index
        all_performance['date'] = all_performance['date'].apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d'))
        all_performance.index = all_performance['date']

        idicators = {}

        # 策略的最终收益率及年化收益率

        idicators['strategy_returns'] = all_performance.iloc[-1]['equity_curve'] - 1
        idicators['annual_strategy_returns'] = idicators['strategy_returns'] * 365 / len(all_performance)
        # 基准的最终收益率及年化收益率
        idicators['benchmark_returns'] = all_performance.iloc[-1]['benchmark_equity_curve'] - 1
        idicators['annual_benchmark_returns'] = idicators['benchmark_returns'] * 365 / len(all_performance)
        idicators['sharpe_ratio'] = create_sharpe_ratio(all_performance['returns'])
        idicators['mdd'], idicators['dd_duration'] = create_drawdowns(all_performance['equity_curve'])
        idicators['volatility'] = np.std(all_performance['returns'])

        print 'all_performance:'

        return all_performance, idicators


    def create_equity_curve_dataframe(self):
        """
        Creates a pandas DataFrame from the all_holdings
        list of dictionaries.
        """
        curve = pd.DataFrame(self.all_holdings)
        #curve.set_index('datetime', inplace=True)
        curve.set_index('date', inplace=True)
        #curve.set_index('datetime')
        curve['returns'] = curve['total'].pct_change()
        curve['equity_curve'] = (1.0 + curve['returns']).cumprod()
        self.equity_curve = curve


    def output_summary_stats(self):
        """
        Creates a list of summary statistics for the portfolio such
        as Sharpe Ratio and drawdown information.
        """
        total_return = self.equity_curve['equity_curve'][-1]
        returns = self.equity_curve['returns']
        pnl = self.equity_curve['equity_curve']

        sharpe_ratio = create_sharpe_ratio(returns)
        max_dd, dd_duration = create_drawdowns(pnl)

        stats = [("Total Return", "%0.2f%%" % ((total_return - 1.0) * 100.0)),
                 ("Sharpe Ratio", "%0.2f" % sharpe_ratio),
                 ("Max Drawdown", "%0.2f%%" % (max_dd * 100.0)),
                 ("Drawdown Duration", "%d" % dd_duration)]

        print stats
        #print pd.DataFrame.from_dict(self.all_holdings)
        #print pd.DataFrame.from_dict(self.all_positions)
        #print pd.DataFrame.from_dict(self.equity_curve)

        return stats

    def show_summary_stats(self):

        df_all_holdings = pd.DataFrame.from_dict(self.all_holdings)
        df_all_positions = pd.DataFrame.from_dict(self.all_positions)
        df_equity_curve = pd.DataFrame.from_dict(self.equity_curve)

        df_all_holdings.to_csv(QBConfigIO.FOLDER_PATH_OUTPUT + 'all_holdings.csv', encoding='GBK')
        df_all_positions.to_csv(QBConfigIO.FOLDER_PATH_OUTPUT + 'all_positions.csv', encoding='GBK')
        df_equity_curve.to_csv(QBConfigIO.FOLDER_PATH_OUTPUT + 'equity_curve.csv', encoding='GBK')

        #df_total = df_all_holdings['total']
        #df_total.plot(figsize=(10, 6))
        #plt.ylabel('PnL')

        df_equity_curve_show = df_equity_curve['equity_curve']
        df_equity_curve_show.plot(figsize=(10, 6))
        plt.ylabel('equity_curve')


        pass


