# -*- coding: utf-8 -*-

import pandas as pd


class QBLoader(object):

    def __init__(self, filename, flag_encoding):
        self.__filename = filename
        self.__flag_encoding = flag_encoding
        print u'\n>>>DBLoader: Initialized.'

    def loadto_df(self):
        if self.__flag_encoding == 1:
            df = pd.read_csv(self.__filename, encoding='GBK')
            #self.__df_data.to_csv(self.__filename, encoding='GBK')
            print u'\n>>>DBLoader: load to ' + self.__filename + ' with GBK.'
            return df
        else:
            df = pd.read_csv(self.__filename)
            print u'\n>>>DBLoader: load to ' + self.__filename + ' with unicode.'
            return df
