# -*- coding: utf-8 -*-
import queue
from QBData import CSVDataHandler
from QBPortfolio import SimplePortfolio
from QBExecution import SimulatedExecutionHandler
from QBStrategy import BuyAndHoldStrategy, BuySpecifiedAndHoldStrategy
from QBConfigIO import FOLDER_PATH_HISTORY_PRICE

class Backtest(object):

    def __init__(self,strategy_cls):

        self.events = queue.Queue()

        #self._data_handler = CSVDataHandler()
        #print self.events.qsize()
        self._universe = ['600000', '600036', '600519', '601166', '601939']

        self._data_handler = CSVDataHandler(self.events, FOLDER_PATH_HISTORY_PRICE, self._universe)
        #print self.events.qsize()

        #self._portfolio = Portfolio()
        self._portfolio = SimplePortfolio(self._data_handler, self.events)

        #self._execution = ExecutionHandler()
        self._execution = SimulatedExecutionHandler(self.events)

        #self._strategy = strategy_cls(self._data_handler,self.events)
        #self._strategy = BuyAndHoldStrategy(self._data_handler,self.events)
        self._strategy = BuySpecifiedAndHoldStrategy(self._data_handler, self.events, self._portfolio)


        self._init_event_handlers()

    #挂载事件，每个事件类型对应一个处理函数

    def _init_event_handlers(self):

        self._event_handler = {}
        self._event_handler['BAR'] = self._handle_event_bar
        self._event_handler['SIGNAL'] = self._handle_event_signal
        self._event_handler['ORDER'] =  self._handle_event_order
        self._event_handler['FILL'] = self._handle_event_fill

    #主系统入口

    def run(self):

        while True:

            #每次loop，都会让data_handler触发新的bar事件，即有一个新bar到达
            #b_continue_backtest是数据管理器的标志，置False则回测停止

            if self._data_handler.b_continue_backtest:
                print 'size of events:', self.events.qsize()
                print 'execute _data_handler.update_bars()'
                self._data_handler.update_bars()
                print 'size of events:', self.events.qsize()

            else:
                break

            #可能有多事件，要处理完，如果队列暂时是空的，不处理
            while True:

                #队列为空则消息循环结束
                #if len(self.events) == 0:
                if self.events.empty():
                    break

                else:

                    event = self.events.get(block=False)
                    self._handle_event(event)

    def show_results(self):
        self._portfolio.create_equity_curve_dataframe()
        self._portfolio.output_summary_stats()
        self._portfolio.show_summary_stats()
        pass

    def _handle_event(self,event):

        #print 'event:', event
        handler = self._event_handler.get(event.type,None)

        if handler is None:
            print('type:%s,handler is None'%event.type)

        else:
            handler(event)
            print '------------------------------------------------------'

    #处理每个时间周期的bar事件
    def _handle_event_bar(self,event):

        print('OnBar Event',event.type)
        #self._strategy.onbar(event)
        self._strategy.onbar()
        self._portfolio.update_timeindex()

    #处理策略产生的交易信号
    def _handle_event_signal(self,event):

        print('OnSignal Event', event.type, event.symbol, event.signal_type)
        self._portfolio.on_signal(event)


    # 处理ORDER
    def _handle_event_order(self, event):

        print('OnOrder Event', event.type, event.symbol)
        self._execution.execute_order(event)

    # 处理FILL
    def _handle_event_fill(self, event):

        print('OnFill Event', event.type, event.symbol)
        self._portfolio.on_fill(event)


bt = Backtest(1)
bt.run()
bt.show_results()