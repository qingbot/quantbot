# -*- coding: utf-8 -*-

import time

STR_DAY_CURRENT = time.strftime("%Y%m%d", time.localtime())

FILE_PATH_DAY_PRICE = 'QBdata\\DayPrice\\dp' + STR_DAY_CURRENT + '.csv'

FILE_PATH_INDUSTRY = 'QBData\\Industry\\in' + STR_DAY_CURRENT + '.csv'
FILE_PATH_INDUSTRY_STOCKINFO = 'QBData\\IndustryStockInfo\\ins' + STR_DAY_CURRENT + '.csv'
FILE_PATH_INDUSTRYINFO = 'QBData\\IndustryInfo\\ini' + STR_DAY_CURRENT + '.csv'

FILE_PATH_PROFIT_FORECAST = 'QBData\\ProfitForecast\\pf' + STR_DAY_CURRENT + '.csv'
FILE_PATH_RESULT_FORECAST = 'QBData\\ResultForecast\\rf' + STR_DAY_CURRENT + '.csv'

FILE_PATH_STOCK_LIST = 'QBData\\StockList\\sl' + STR_DAY_CURRENT + '.csv'

FILE_LIST_MAIL = [FILE_PATH_RESULT_FORECAST,
                  FILE_PATH_INDUSTRY_STOCKINFO,
                  FILE_PATH_INDUSTRYINFO
                  ]

FILE_PATH_INDUSTRY_JQ = 'QBData\\IndustryClass\\industry_stock_joinquant.csv'

FOLDER_PATH_HISTORY_PRICE = 'QBData\\HistoryPrice\\'
FOLDER_PATH_OUTPUT = 'QBData\\Output\\'
FOLDER_PATH_REPORT = 'QBData\\Report\\'
FOLDER_PATH_PROFIT = 'QBData\\Profit\\'

DB_PATH_QINGBOT = 'mysql://root:abc_1234@127.0.0.1/qingbot_gbk?charset=GBK'
DB_TABLE_NAME_RESULT_FORECAST = 'stock_forecast'
DB_TABLE_NAME_INDUSTRY_STOCKINFO = 'industry_stockinfo'
DB_TABLE_NAME_INDUSTRYINFO = 'industryinfo'