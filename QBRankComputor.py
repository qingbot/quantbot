# -*- coding: utf-8 -*-

import QBConfigIO
import time
from sqlalchemy import create_engine

class QBRankComputor(object):

    def __init__(self, df):
        self.__df_data = df
        print u'\n>>>QBRandComputor: Initialized.'

    def compute_df(self):
        print u'\n>>>QBRandComputor: Compute DF.'
        df_stock = self.__df_data
        df_stock['p2018_25'] = df_stock['e2018'] * 25
        df_stock['p2018_20'] = df_stock['e2018'] * 20
        df_stock['tms_25'] = df_stock['p2018_25'] / (df_stock['trade'] + 0.00001)
        df_stock['tms_20'] = df_stock['p2018_20'] / (df_stock['trade'] + 0.00001)
        df_stock['tms_growth'] = df_stock['e2018'] / (df_stock['e2015'] + 0.00001)
        rate1 = df_stock['e2016'] / df_stock['e2015'] - 1
        rate2 = df_stock['e2017'] / df_stock['e2016'] - 1
        rate3 = df_stock['e2018'] / df_stock['e2017'] - 1
        print 'type-rate1：', type(rate1)
        # rate1[rate1 > 1] = 1
        # rate2[rate2 > 1] = 1
        # rate3[rate3 > 1] = 1
        rate1[rate1 > 0.5] = 0.5
        rate2[rate2 > 0.5] = 0.5
        rate3[rate3 > 0.5] = 0.5
        df_stock['rate1'] = rate1
        df_stock['rate2'] = rate2
        df_stock['rate3'] = rate3
        rate_forecast = (rate1 + rate2 + rate3) / 3
        df_stock['rate_forecast'] = rate_forecast
        pe_forecast = rate_forecast * 100
        df_stock['pe_forecast'] = pe_forecast
        df_stock['tms_forecast'] = (df_stock['e2018'] * pe_forecast) / (df_stock['trade'] + 0.00001)

        df_stock['pe_forecast_m'] = pe_forecast
        df_mk_pe = df_stock[['mktcap', 'pe_forecast_m']]
        df_mk_pe.loc[(df_mk_pe['mktcap'] > 0) & (df_mk_pe['pe_forecast_m'] > 54), 'pe_forecast_m'] = 54
        df_mk_pe.loc[(df_mk_pe['mktcap'] > 500000) & (df_mk_pe['pe_forecast_m'] > 50), 'pe_forecast_m'] = 50
        df_mk_pe.loc[(df_mk_pe['mktcap'] > 1000000) & (df_mk_pe['pe_forecast_m'] > 45), 'pe_forecast_m'] = 45
        df_mk_pe.loc[(df_mk_pe['mktcap'] > 2000000) & (df_mk_pe['pe_forecast_m'] > 35), 'pe_forecast_m'] = 35
        df_mk_pe.loc[(df_mk_pe['mktcap'] > 4000000) & (df_mk_pe['pe_forecast_m'] > 30), 'pe_forecast_m'] = 20
        df_mk_pe.loc[(df_mk_pe['mktcap'] > 8000000) & (df_mk_pe['pe_forecast_m'] > 20), 'pe_forecast_m'] = 20
        df_stock['pe_forecast_m'] = df_mk_pe['pe_forecast_m']
        df_stock['tms_forecast_m'] = (df_stock['e2018'] * df_stock['pe_forecast_m']) / (df_stock['trade'] + 0.00001)

        df_stock['pe_rate3'] = 100 * df_stock['rate3']
        df_stock['pe_forecast_min'] = df_stock['pe_rate3']
        df_stock.loc[df_stock['pe_forecast_min'] > df_stock['pe_forecast_m'], 'pe_forecast_min'] = df_stock.loc[df_stock['pe_forecast_min'] > df_stock['pe_forecast_m'], 'pe_forecast_m']
        df_stock['tms_forecast_min'] = (df_stock['e2018'] * df_stock['pe_forecast_min']) / (df_stock['trade'] + 0.00001)
        curdate = time.strftime("%Y%m%d", time.localtime())
        df_stock['date'] = curdate

        self.__df_data = df_stock

    def saveto_csv(self):
        self.__df_data.to_csv(QBConfigIO.FILE_PATH_RESULT_FORECAST, encoding='GBK')
        print u'\n>>>QBRandComputor: Save to CSV.'


    def saveto_sql(self):
        engine = create_engine(QBConfigIO.DB_PATH_QINGBOT)
        self.__df_data.to_sql(QBConfigIO.DB_TABLE_NAME_RESULT_FORECAST, engine, if_exists='append')
        print u'\n>>>QBRandComputor: Save to MySQL.'

class QBRankComputorIndustry(object):

    def __init__(self, df):

        self.__df_data = df
        print u'\n>>>QBRankComputorIndustry: Initialized.'

        self.__df_industry_stockinfo, self.____df_industryinfo = self.compute_df()
        print u'\n>>>QBRankComputorIndustry: DFs computed.'

    def compute_df(self):
        print u'\n>>>QBRankComputorIndustry: Compute DFs.'

        df3 = self.__df_data
        # 每行业按照市值排序,默认ascending = True
        df3['mktcap_rank'] = df3.groupby('c_name')['mktcap'].rank()
        # 每行业按照PB排序
        df3['pb_rank'] = df3.groupby('c_name')['pb'].rank()
        # 每行业按照PE排序
        df3['pe_rank'] = df3.groupby('c_name')['per'].rank()
        df3['sum_rank'] = df3['mktcap_rank'] + df3['pb_rank'] + df3['pe_rank']
        df3['comp_rank'] = df3.groupby('c_name')['sum_rank'].rank()


        df_industry_top = df3[df3.comp_rank <= 10]
        df_industry_top['name_x'] += '|'

        groups = df3.groupby('c_name')
        df4 = groups.mean()
        df4 = df4.sort_values(by='pb', ascending=True)
        df4['stock'] = df_industry_top.groupby('c_name')['name_x'].sum()

        df3['date'] = QBConfigIO.STR_DAY_CURRENT
        df4['date'] = QBConfigIO.STR_DAY_CURRENT

        return df3, df4

    def saveto_csv(self):
        self.__df_industry_stockinfo.to_csv(QBConfigIO.FILE_PATH_INDUSTRY_STOCKINFO, encoding='GBK')
        print u'\n>>>QBRankComputorIndustry: Industry StockInfo save to CSV.'

        self.____df_industryinfo.to_csv(QBConfigIO.FILE_PATH_INDUSTRYINFO, encoding='GBK')
        print u'\n>>>QBRankComputorIndustry: IndustryInfo save to CSV.'

    def saveto_sql(self):
        engine = create_engine(QBConfigIO.DB_PATH_QINGBOT)

        self.__df_industry_stockinfo.to_sql(QBConfigIO.DB_TABLE_NAME_INDUSTRY_STOCKINFO, engine, if_exists='append')
        print u'\n>>>QBRankComputorIndustry: Industry StockInfo save to MySQL.'

        self.____df_industryinfo.to_sql(QBConfigIO.DB_TABLE_NAME_INDUSTRYINFO, engine, if_exists='append')
        print u'\n>>>QBRankComputorIndustry: IndustryInfo save to MySQL.'

