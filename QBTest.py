# -*- coding: utf-8 -*-
import QBLoader
import QBConfigIO
import os
import pandas as pd
import queue
import tushare as ts
from opendatatools import economy
from opendatatools import hkex

def fun1():
    obj_qbl_sl = QBLoader.QBLoader(QBConfigIO.FILE_PATH_STOCK_LIST, 1)
    df_sl = obj_qbl_sl.loadto_df()

    col = df_sl.columns.values.tolist()
    print col

def fun2():
    s='600036'

    df_test = pd.read_csv(os.path.join(QBConfigIO.FOLDER_PATH_HISTORY_PRICE, '%s.csv' % s),
                    header=0, index_col='date',parse_dates=False,
                    usecols=[0,1,2,3,4,5],
                    names=['date', 'open', 'high', 'low', 'close', 'volume']
                ).sort_index()
    print df_test


class Cltest1(object):

    def __init__(self, q):
        self.q = q
        print '3.self.q:', self.q
        self._update_q()
        pass

    def _update_q(self):
        self.q.put('newbar')
        print '4.self.q:', self.q
        pass

    def get_q(self):
        result = self.q.get()
        print '8.self.q:', self.q
        return result
def fun3():
    q_test = queue.Queue()
    print '1.size of q_test:', q_test.qsize()
    print '2.q_test:', q_test
    c1 = Cltest1(q_test)
    print '5.execute update q'
    print '6.size of q_test:', q_test.qsize()
    print '7.str_test:', q_test
    c2 = c1.get_q()
    print '9.c2 = ', c2
    print '10.execute get q'
    print '11.size of q_test:', q_test.qsize()
    print '12.str_test:', q_test

'''
df1 = ts.get_hist_data('hs300')
print df1.head()

df2 = ts.get_k_data('000300')
print df2.head()

df_hs300_hist_data = ts.get_hist_data('hs300')
file_hs300 = QBConfigIO.FOLDER_PATH_HISTORY_PRICE + '000300.csv'
df_hs300_hist_data.to_csv(file_hs300)
'''

# df, msg = economy.get_city_map()
# df, msg = economy.get_cpi()
# print df
# print msg

df2 = hkex.get_lgt_share(market='SH', date='2018-06-04')
print df2


