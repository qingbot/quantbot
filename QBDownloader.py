# -*- coding: utf-8 -*-
import tushare as ts
import pandas as pd
import QBConfigIO
import QBLoader
import QBMonitor
import os


class QBDownloader(object):

    def __init__(self):
        self._filename = ''
        #self.__df_data = self.get_df_data()
        self._df_data = pd.DataFrame()

        self._flag_encoding = 1

    def saveto_csv(self):

        if os.path.exists(self._filename) == False:

            self._df_data = self.get_df_data()

            if self._flag_encoding == 1:
                self._df_data.to_csv(self._filename, encoding='GBK')
                print u'\n>>>QBDownloader: save ' + self._filename + u' with GBK.'
            else:
                self._df_data.to_csv(self._filename)
                print u'\n>>>QBDownloader: save ' + self._filename + u' with unicode.'
        else:
            print u'\n>>>QBDownloader: ' + self._filename + u' already exists.'


    def get_df_data(self):
        pass
    

class QBDownloaderDayPrice(QBDownloader):

    def __init__(self):
        super(QBDownloaderDayPrice, self).__init__()
        self._filename = QBConfigIO.FILE_PATH_DAY_PRICE
        pass


    def get_df_data(self):
        #获取当天所有股票日价格，返回dataframe
        print u'\n>>>QBDownloader: get all price of today.'
        df = ts.get_today_all()
        return df


class QBDownloaderIndustry(QBDownloader):

    def __init__(self):
        super(QBDownloaderIndustry, self).__init__()
        self._filename = QBConfigIO.FILE_PATH_INDUSTRY
        pass

    def get_df_data(self):
        #获取当天所有股票日价格，返回dataframe
        print u'\n>>>QBDownloader: get classified industry.'
        df = ts.get_industry_classified()
        return df

class QBDownloaderStockList(QBDownloader):

    def __init__(self):
        super(QBDownloaderStockList, self).__init__()
        self._filename = QBConfigIO.FILE_PATH_STOCK_LIST
        pass

    def get_df_data(self):
        #获取当天所有股票日价格，返回dataframe
        print u'\n>>>QBDownloader: get stock list.'
        df = ts.get_stock_basics()
        return df

class QBDownloaderReport(QBDownloader):

    def __init__(self, year, quarter):
        super(QBDownloaderReport, self).__init__()
        self._year = year
        self._quarter = quarter
        self._filename = QBConfigIO.FOLDER_PATH_REPORT + 'rp' + str(self._year) + '0' + str(self._quarter) + '.csv'
        pass

    def get_df_data(self):
        #获取当天所有股票日价格，返回dataframe
        print u'\n>>>QBDownloader: get report data.'
        df = ts.get_report_data(self._year, self._quarter)
        return df


class QBDownloaderProfit(QBDownloader):

    def __init__(self, year, quarter):
        super(QBDownloaderProfit, self).__init__()
        self._year = year
        self._quarter = quarter
        self._filename = QBConfigIO.FOLDER_PATH_PROFIT + 'pf' + str(self._year) + '0' + str(self._quarter) + '.csv'
        pass

    def get_df_data(self):
        #获取当天所有股票日价格，返回dataframe
        print u'\n>>>QBDownloader: get profit data.'
        df = ts.get_profit_data(self._year, self._quarter)
        return df


class QBDownloaderFundamentalBach():

    def __init__(self):
        # 从2015年开始
        self._ls_year_list = range(2015, 2018)
        # 一至四季度
        self._ls_quarter_list = range(1, 5)

    def download_bach(self):

        for every_year in self._ls_year_list:
            for every_quarter in self._ls_quarter_list:

                obj_dl = QBDownloaderReport(every_year, every_quarter)
                obj_dl.saveto_csv()

                obj_dl = QBDownloaderProfit(every_year, every_quarter)
                obj_dl.saveto_csv()
        pass


if __name__ == '__main__':

    obj_dl = QBDownloaderDayPrice()
    obj_dl.saveto_csv()


    obj_dl = QBDownloaderIndustry()
    obj_dl.saveto_csv()

    obj_dl = QBDownloaderStockList()
    obj_dl.saveto_csv()

    print u'存储成功'

    obj_qbl_sl = QBLoader.QBLoader(QBConfigIO.FILE_PATH_STOCK_LIST, 1)
    df_sl = obj_qbl_sl.loadto_df()
    obj_qbm = QBMonitor.QBMonitor(df_sl)
    obj_qbm.his_price_of_pool_saveto_csv()

    obj_dl_bach = QBDownloaderFundamentalBach()
    obj_dl_bach.download_bach()
